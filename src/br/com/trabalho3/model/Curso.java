package br.com.trabalho3.model;


import br.com.trabalho3.connection.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Curso {

    protected int id;
    protected String turno;
    protected Double duracao;
    protected String ppc;
    protected ArrayList <Aluno> alunosDoCurso = new ArrayList();

 
    public Curso() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }
    
   

    public Double getDuracao() {
        return duracao;
    }

    public void setDuracao(Double duracao) {
        this.duracao = duracao;
    }

    public String getPpc() {
        return ppc;
    }

    public void setPpc(String ppc) {
        this.ppc = ppc;
    }

    public ArrayList<Aluno> getAlunosDoCurso() {
        return alunosDoCurso;
    }

    public void setAlunosDoCurso(ArrayList<Aluno> alunosDoCurso) {
        this.alunosDoCurso = alunosDoCurso;
    }
    
    
    public boolean insert(){
        
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String demp = "select max (codigo_curso) as id from curso";
        try{
            preparedStatement = dbConnection.prepareStatement(demp);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()){
                this.id = rs.getInt("id") +1;
            }
        }catch(SQLException e){
    
            System.out.println("erro"+e);
        }
        
        String insertTableSQL = "INSERT INTO Curso" + "(codigo_curso, turno, duracao, ppc) VALUES" + "(?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, this.id);
            preparedStatement.setString(2, this.turno);
            preparedStatement.setDouble(3, this.duracao);
            preparedStatement.setString(4, this.ppc);


            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
}