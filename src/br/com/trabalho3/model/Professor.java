package br.com.trabalho3.model;


import br.com.trabalho3.connection.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Professor extends Usuario {
    
    private String area;
    private Double salario;
    private Date dataIngresso;
    private String curriculo;
    private int codigoProfessor;
    public Professor() {
        
    }

    public int getCodigoProfessor() {
        return codigoProfessor;
    }

    public void setCodigoProfessor(int codigoProfessor) {
        this.codigoProfessor = codigoProfessor;
    }
    
    

    
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public Date getDataIngresso() {
        return dataIngresso;
    }

    public void setDataIngresso(Date dataIngresso) {
        this.dataIngresso = dataIngresso;
    }

    public String getCurriculo() {
        return curriculo;
    }

    public void setCurriculo(String curriculo) {
        this.curriculo = curriculo;
    }
    
    public boolean insert() {
         
            Conexao c = new Conexao();
            Connection dbConnection = c.getConexao();
            PreparedStatement preparedStatement = null;
            String demp = "select max (codigo_professor) as id from professor";
                    try{
                        preparedStatement = dbConnection.prepareStatement(demp);
                        ResultSet rs = preparedStatement.executeQuery();
                        if (rs.next()){
                            this.codigoProfessor = rs.getInt("id") +1;
                        }
                    }catch(SQLException e){
                        System.out.println("erro"+e);
                    }

            String insertTableSQL = "INSERT INTO Professor" + "(codigo_Professor, matricula, senha, email, nome, endereco) VALUES" + "(?,?,?,?,?,?)";


            try {
                preparedStatement = dbConnection.prepareStatement(insertTableSQL);
                preparedStatement.setInt(1, this.codigoProfessor);
                preparedStatement.setString(2, matricula);
                preparedStatement.setString(3, senha);
                preparedStatement.setString(4, email);
                preparedStatement.setString(5, nome);
                preparedStatement.setString(6, endereco);


                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }finally{
                c.desconecta(); 
            }        
            return true;
        }
    
    public boolean modificarReferencia(Disciplinas d) {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE disciplinas SET referencias =? WHERE id = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, d.getId());
            ps.setString(2, d.getReferencias());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }        
        return true;
    }
    
    public boolean modificarObjetivos(Disciplinas d) {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE disciplinas SET objetivos =? WHERE id = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, d.getId());
            ps.setString(2, d.getObjetivos());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }        
        return true;
    }
    public boolean modificarCronograma(Disciplinas d) {
      	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        String insertTableSQL = "UPDATE disciplinas SET conteudo =? WHERE id = ?";
        

        try {
            ps = dbConnection.prepareStatement(insertTableSQL);
            ps.setInt(1, d.getId());
            ps.setString(2, d.getConteudo());

            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta();
        }        
        return true;
    }
}