package br.com.trabalho3.model;


import br.com.trabalho3.connection.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TecnicoAdmin extends Usuario {

    private String setor;
    private Double salario;
    private int codigoAdmin;

    public TecnicoAdmin() {
        
    }

    public int getCodigoAdmin() {
        return codigoAdmin;
    }

    public void setCodigoAdmin(int codigoAdmin) {
        this.codigoAdmin = codigoAdmin;
    }
    
    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }
    
    public boolean insert() {
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String demp = "select max (codigo_admin) as id from admin";
                try{
                    preparedStatement = dbConnection.prepareStatement(demp);
                    ResultSet rs = preparedStatement.executeQuery();
                    if (rs.next()){
                        this.codigoAdmin = rs.getInt("id") +1;
                    }
                }catch(SQLException e){
                    System.out.println("erro"+e);
                }

        String insertTableSQL = "INSERT INTO Admin" + "(codigo_admin, matricula, senha, email, nome, endereco) VALUES" + "(?,?,?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, this.codigoAdmin);
            preparedStatement.setString(2, matricula);
            preparedStatement.setString(3, senha);
            preparedStatement.setString(4, email);
            preparedStatement.setString(5, nome);
            preparedStatement.setString(6, endereco);


            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
}