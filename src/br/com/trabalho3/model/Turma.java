package br.com.trabalho3.model;


import br.com.trabalho3.connection.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Turma {

    private int id;
    private String horario;
    private String prova;
    private String chamada;

    public Turma() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getProva() {
        return prova;
    }

    public void setProva(String prova) {
        this.prova = prova;
    }
    


    public String getChamada() {
        return chamada;
    }

    public void setChamada(String chamada) {
        this.chamada = chamada;

    }

    public boolean insert(){
        
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String demp = "select max (codigo_turma) as id from turma";
        try{
            preparedStatement = dbConnection.prepareStatement(demp);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()){
                this.id = rs.getInt("id") +1;
            }
        }catch(SQLException e){
    
            System.out.println("erro"+e);
        }
        String insertTableSQL = "INSERT INTO turma" + "(codigo_turma, horario, prova, chamada) VALUES" + "(?,?,?,?)";
        

        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, this.id);
            preparedStatement.setString(2,this.horario);
            preparedStatement.setString(3, this.prova);
            preparedStatement.setString(4, this.chamada);


            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        return true;
    }
}
    
