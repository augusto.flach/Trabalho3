package br.com.trabalho3.model;


import br.com.trabalho3.connection.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Aluno extends Usuario {
  
    private Integer anoPrimeiraMatricula;
    private Boolean status;
    private int codigoAluno;
    
    public Aluno() {
        
    }

    public int getCodigoAluno() {
        return codigoAluno;
    }

    public void setCodigoAluno(int codigoAluno) {
        this.codigoAluno = codigoAluno;
    }

    public Integer getAnoPrimeiraMatricula() {
        return anoPrimeiraMatricula;
    }

    public void setAnoPrimeiraMatricula(Integer anoPrimeiraMatricula) {
        this.anoPrimeiraMatricula = anoPrimeiraMatricula;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

        public boolean insert() {
         
            Conexao c = new Conexao();
            Connection dbConnection = c.getConexao();
            PreparedStatement preparedStatement = null;
            String demp = "select max (codigo_aluno) as id from aluno";
                    try{
                        preparedStatement = dbConnection.prepareStatement(demp);
                        ResultSet rs = preparedStatement.executeQuery();
                        if (rs.next()){
                            this.codigoAluno = rs.getInt("id") +1;
                        }
                    }catch(SQLException e){
                        System.out.println("erro"+e);
                    }

            String insertTableSQL = "INSERT INTO aluno" + "(codigo_aluno, matricula, senha, email, nome, endereco) VALUES" + "(?,?,?,?,?,?)";


            try {
                preparedStatement = dbConnection.prepareStatement(insertTableSQL);
                preparedStatement.setInt(1, this.codigoAluno);
                preparedStatement.setString(2, matricula);
                preparedStatement.setString(3, senha);
                preparedStatement.setString(4, email);
                preparedStatement.setString(5, nome);
                preparedStatement.setString(6, endereco);


                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }finally{
                c.desconecta(); 
            }        
            return true;
        }
}