package br.com.trabalho3.model;


import br.com.trabalho3.connection.Conexao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Disciplinas {

    private int id;
    private String nome;
    private String ementa;
    private String objetivos;
    private String conteudo;
    private String referencias;

    public Disciplinas() {
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public String getObjetivos() {
        return objetivos;
    }

    public void setObjetivos(String objetivos) {
        this.objetivos = objetivos;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }

    public String getReferencias() {
        return referencias;
    }

    public void setReferencias(String referencias) {
        this.referencias = referencias;
    }
    
    public boolean insert(){
        
	Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement preparedStatement = null;
        String demp = "select max (codigo_disciplina) as id from disciplina";
        try{
            preparedStatement = dbConnection.prepareStatement(demp);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()){
                this.id = rs.getInt("id") +1;
            }
        }catch(SQLException e){
    
            System.out.println("erro"+e);
        }
        String insertTableSQL = "INSERT INTO disciplina" + "(codigo_disciplina, conteudo, ementa, objetivos, nome, referencias) VALUES" + "(?,?,?,?,?,?)";
        try {
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setInt(1, this.id);
            preparedStatement.setString(2, this.conteudo);
            preparedStatement.setString(3, this.ementa);
            preparedStatement.setString(4, this.objetivos);
            preparedStatement.setString(5, this.nome);
            preparedStatement.setString(6, this.referencias);


            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally{
            c.desconecta(); 
        }        
        return true;
    }

}