/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.connection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class Helper {

    public static ResultSet load(String sql) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        ResultSet rs;

        try {
            ps = dbConnection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void insert(String sql, String idT, String idT2) {

        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        ResultSet rs;
        int id;
        try {
            ps = dbConnection.prepareStatement(idT);
            rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt(idT2);
            }
        } catch (SQLException ex) {
        }

        try {
            ps = dbConnection.prepareStatement(sql);

            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }

    }

    public static void delete(String sql) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        try {
            ps = dbConnection.prepareStatement(sql);

            // observe que uso o método execute update
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void update(String sql) {
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;

        try {
            ps = dbConnection.prepareStatement(sql);

            // observe que uso o método execute update
            ps.executeUpdate();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
