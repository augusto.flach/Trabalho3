/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.connection;

/**
 *
 * @author Aluno
 */
public interface ActiveRecord {

    public boolean insert();
    
    public boolean delete(); 
    
    public boolean update(); 
    
    public void load();
  
}