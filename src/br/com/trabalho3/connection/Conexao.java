/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Loser's Club <clubedosperdedores@hotgmail.gon>
 * @version 1.0.0
 */
public class Conexao {

    private String usuario = "bd3_int02";
    private String senha = "13121999";
    private String servidor = "oracle.canoas.ifrs.edu.br";
    private int porta = 1521;

    private Connection conexao = null;

    public Conexao() {
    }//inicia com os valores padrões

    public Conexao(String usuario, String senha) {
        this.senha = senha;
        this.usuario = usuario;
    }

    public Connection getConexao() {
        if (conexao == null) {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
                conexao = DriverManager.getConnection(
                        "jdbc:oracle:thin:@" + this.servidor + ":" + this.porta + ":XE",
                        this.usuario,
                        this.senha);
            } catch (ClassNotFoundException e) {
                System.out.println("Mas o que é isso? Mas o que é isso? Mas o que é isso aqui!?");
            } catch(SQLException e){
                e.printStackTrace(); //Mas o que é isso mas o que é isso mas o que é isso aqui.
            }
        }
        return conexao;
    }

    public void desconecta() {
        try {
            conexao.close();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
