/* To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.trabalho3.view;

import br.com.trabalho3.connection.Helper;
import java.io.IOException;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import br.com.trabalho3.view.TelaLoginController;
import javafx.scene.Parent;

/**
 *
 * @author gamificacao
 */
public class Trabalho3 extends Application {

    static Stage stage = new Stage();

    
        
        @Override
        
	public void start(Stage primaryStage) throws IOException {
           
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Tela de Login.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("JEGUXIEGAS");
            stage.show();
            
        }

        
	public void trocaTela (String s) throws IOException {
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/trabalho3/view/"+s+".fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setTitle("JEGUXIEGAS");
            stage.show();
	}   
   
}