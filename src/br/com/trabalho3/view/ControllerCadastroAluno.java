/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.view;

import br.com.trabalho3.model.Aluno;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 *
 * @author lucia
 */
public class ControllerCadastroAluno implements Initializable {

    @FXML
    TextField tfNome;
    @FXML
    TextField tfEmail;
    @FXML
    TextField tfEndereco;
    @FXML
    TextField tfMatricula;
    @FXML
    PasswordField pfSenha;
        
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    @FXML
    public void buttonCadastrar(){
        
        Aluno ta = new Aluno();
        
        ta.setNome(tfNome.getText());
        ta.setEmail(tfEmail.getText());
        ta.setEndereco(tfEndereco.getText());
        ta.setSenha(pfSenha.getText());
        ta.setMatricula(tfMatricula.getText());
        ta.insert();
    }
    
}
