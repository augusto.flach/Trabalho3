/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.view;

import br.com.trabalho3.model.Turma;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 *
 * @author lucia
 */
public class ControllerCadastroTurma implements Initializable{

    @FXML
    private TextField tfChamada;
    @FXML
    private TextField tfHorario;
    @FXML
    private TextField tfProva;

    
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    
    @FXML
    public void cadastrar(){
        
        Turma t = new Turma();
        t.setChamada(tfChamada.getText());
        t.setHorario(tfHorario.getText());
        t.setProva(tfProva.getText());
        t.insert();
    }
    
}
