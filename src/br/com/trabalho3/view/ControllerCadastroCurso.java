/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.view;

import br.com.trabalho3.model.Curso;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 *
 * @author lucia
 */
public class ControllerCadastroCurso implements Initializable {

    @FXML
    private TextField tfPpc;
    @FXML
    private TextField tfDuracao;
    @FXML
    private TextField tfTurno;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    
    @FXML
    public void cadastrar(){
        
        Curso curso = new Curso();
        curso.setDuracao(Double.parseDouble(tfDuracao.getText()));
        curso.setPpc(tfPpc.getText());
        curso.setTurno(tfTurno.getText());
        curso.insert();
    }
    
}
