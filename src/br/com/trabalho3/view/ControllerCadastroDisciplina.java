/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.view;

import br.com.trabalho3.model.Disciplinas;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 *
 * @author lucia
 */
public class ControllerCadastroDisciplina implements Initializable {

    @FXML
    private TextField tfNome;
    @FXML
    private TextField tfConteudo;
    @FXML
    private TextField tfEmenta;
    @FXML
    private TextField tfObjetivos;
    @FXML
    private TextField tfReferencia;

    
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    
    @FXML
    public void cadastrar(){
        
        Disciplinas d = new Disciplinas();
        d.setConteudo(tfConteudo.getText());
        d.setNome(tfNome.getText());
        d.setEmenta(tfEmenta.getText());
        d.setObjetivos(tfObjetivos.getText());
        d.setReferencias(tfReferencia.getText());
        d.insert();
    }
}
