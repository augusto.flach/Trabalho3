/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 *
 * @author lucia
 */
public class ControllerCadastroGeral implements Initializable {
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
   
    @FXML
    public void buttonProfessor2() throws IOException{
    
        Trabalho3 t = new Trabalho3();
        t.trocaTela("TelaCadastroProfessor");
    }
    @FXML
    public void buttonAluno() throws IOException{
    
        Trabalho3 t = new Trabalho3();
        t.trocaTela("Tela de Cadastrar um novo cadastro");
    }
    @FXML
    public void buttonAdmin() throws IOException{
    
        Trabalho3 t = new Trabalho3();
        t.trocaTela("Tela de Cadastrar um otario administrativo");
    }
    
}
