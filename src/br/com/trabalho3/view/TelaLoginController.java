/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.view;

import br.com.trabalho3.connection.Helper;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;

/**
 * FXML Controller class
 *
 * @author gamificacao
 */
public class TelaLoginController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private TextField id;
    @FXML
    private TextField senha;

    Trabalho3 t3 = new Trabalho3();

    @FXML
    private void logar() throws IOException {

        String id = this.id.getText();
        String senha = this.senha.getText();
        if(checarLoginU(id,senha)){
            t3.trocaTela("Tela Inicial de um novo cadastro");
            System.out.println("Aluno Logado");
        }
        else{
            if (checarLoginF(id, senha)) {
                t3.trocaTela("Tela Inicial de um otario administrativo");
                System.out.println("Funcionario Logado");
            }
            else
            {
                if(checarLoginP(id,senha)){
                    t3.trocaTela("Tela Inicial de um otario administrativo");
                    System.out.println("Funcionario Logado");
                }
            }
        }
    }

    @FXML
    public void cadastrar() throws IOException {
        Trabalho3 t = new Trabalho3();
        t.trocaTela("Ainda nao se cadastrou");
    }

    @FXML
    private boolean checarLoginU(String id, String senha) {

        String sql = "select * from aluno where matricula = '" + id+"'";
        ResultSet rs = Helper.load(sql);
        if (rs != null) {
            try {
                if (rs != null) {
                    if (senha.equals(rs.getString("senha"))) {
                        return true;
                    }
                    return false;
                }
                return false;
            } catch (SQLException ex) {
                Logger.getLogger(TelaLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @FXML
    private boolean checarLoginF(String id, String senha) {

        String sql = "select * from admin where matricula = '" + id+"'";
        ResultSet rs = Helper.load(sql);
        if (rs != null) {

            try {
                if (rs != null) {
                    if (senha.equals(rs.getString("senha"))) {
                        return true;
                    }
                    return false;
                }
                return false;
            } catch (SQLException ex) {
                Logger.getLogger(TelaLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @FXML
    private boolean checarLoginP(String id, String senha) {

        String sql = "select * from PROFESSOR where matricula = '" + id+"'";
        ResultSet rs = Helper.load(sql);
        if (rs != null) {

            try {
                if (rs != null) {
                    if (senha.equals(rs.getString("senha"))) {
                        return true;
                    }
                    return false;
                }
                return false;
            } catch (SQLException ex) {
                Logger.getLogger(TelaLoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

}
