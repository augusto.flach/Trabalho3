/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.trabalho3.view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 *
 * @author lucia
 */
public class ControllerTelaInicialAdm implements Initializable{

    Trabalho3 t3 = new Trabalho3();
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    @FXML
    public void button1() throws IOException{
        
        t3.trocaTela("Tela de Cadastrar nova turma");
    }
    @FXML
    public void button2() throws IOException{
        
        t3.trocaTela("Tela de Cadastrar nova disciplina");

    }
    @FXML
    public void button3() throws IOException{
        
        t3.trocaTela("Tela de Cadastrar novo curso");
    }
    
    
    
}
